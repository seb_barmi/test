import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import AdressText from '../AdressText';

const Address = () => {
    return (
        <View style={styles.address}>
            <View style={styles.selectAddress}>
                <AdressText style={styles.addressText} text='Анкудиновка, Свободы, 5к2'/>
                <Image style={styles.addressImage} source={require('../../assests/icons/Shape.png')}/>
            </View>
            <Image style={styles.ourAccount} source={require('../../assests/icons/icUser.png')}/>
        </View>
    );
};




const styles = StyleSheet.create({
    address: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    selectAddress: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    addressText: {
        marginRight: 9.75,
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 17,
        letterSpacing: -0.32,
        color: '#353539',
    },
    addressImage: {
        width: 7.5,
        height: 3.75
    },
    ourAccount: {
        width: 20,
        height: 20
    }
});

export default Address;