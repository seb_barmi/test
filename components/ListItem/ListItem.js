import React from 'react'

import { View, Text, ImageBackground, StyleSheet } from 'react-native';


const image = require('../../assests/storyImage/2.png')

const ListItem = ({item: {text, imageURL}}) => {

    

    return (
        <View style={styles.listItem}>
            <ImageBackground source={imageURL} resizeMode="cover" imageStyle={{borderRadius: 12}} style={styles.image}>
                <Text style={styles.listItemText}>{text}</Text>
            </ImageBackground>
        </View>
    );
};

const styles = StyleSheet.create({
    listItem: {
        width: 98,
        height: 98,
        borderRadius: 12,
        borderColor: 'black',
        marginRight: 12
    },
    image: {
        width: '100%',
        height: '100%',
        flexDirection: 'row'
    },
    listItemText: {
        alignSelf: 'flex-end',
        justifyContent: 'center',
        flexDirection: 'column',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 14,
        color: '#FFFFFF',
        paddingHorizontal: 8,
        paddingBottom: 8
    }
});

export default ListItem;