import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Address from '../Address';
import Order from '../Order';
import Search from '../Search';
import Stories from '../Stories';
import Menu from '../Menu';
import ShoppingCartBtn from '../ShoppingCartBtn';

const HomeScreen = () => {
  const [isVisibleCart, setVisibleCart] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Address />
        <Stories />
        <Search />
        <Order />
        <Menu />
      </ScrollView>
      {isVisibleCart && (
        <ShoppingCartBtn style={styles.shopCart} price="150 ₽" />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    position: 'relative',
  },
  shopCart: {
    position: 'absolute',
    bottom: 29,
    right: 16,
  },
});

export default HomeScreen;
