import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ListStory from '../ListStory';
import Title from '../Title';

const Stories = () => {
  return (
    <View style={styles.stories}>
      <Title text="Истории" />
      <ListStory />
    </View>
  );
};

const styles = StyleSheet.create({
  stories: {
    marginTop: 27,
  },
});

export default Stories;
