import React from 'react';

import {View, Text, StyleSheet} from 'react-native';
import MenuList from '../MenuList';
import Title from '../Title';

const Menu = () => {
  return (
    <View style={styles.menu}>
      <Title text="Напитки" />
      <MenuList />
    </View>
  );
};

const styles = StyleSheet.create({
  menu: {
    marginTop: 24,
  },
});

export default Menu;
