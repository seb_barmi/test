import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const OrderListItem = ({item: {name, price, size}}) => {
    return (
        <View style={styles.orderListItem}>
            <View style={styles.orderListItemHead}>
                <Text style={styles.orderListItemName}>{name}</Text>
                <Text style={styles.orderListItemPrice}>{price} ₽</Text>
            </View>
            <Text style={styles.orderListItemSize}>{size}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    orderListItemHead: {
        marginTop: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    orderListItemName: {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 17,
        letterSpacing: -0.32,
        color: '#353539'
    },
    orderListItemPrice: {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 17,
        letterSpacing: -0.32,
        color: '#989FA6',
    },
    orderListItemSize: {
        marginTop: 4,
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 13,
        lineHeight: 18,
        letterSpacing: -0.08,
        color: '#989FA6',
    }
});

export default OrderListItem;