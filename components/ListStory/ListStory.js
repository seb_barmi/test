import React from 'react';
import {View, Text, FlatList} from 'react-native';
import ListItem from '../ListItem';
import dataCoffee from '../../services/dataService';

const ListStory = () => {
  const dataWithCoffee = new dataCoffee();

  return (
    <FlatList
      data={dataWithCoffee.getStories()}
      renderItem={ListItem}
      keyExtractor={item => item.id}
      horizontal
      style={{marginTop: 20}}
      showsHorizontalScrollIndicator={false}
    />
  );
};

export default ListStory;
