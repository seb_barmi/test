import * as React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import HomeScreen from '../HomeScreen';
import Translator from '../Translator';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Верстка" component={HomeScreen} />
        <Drawer.Screen name="Переводчик" component={Translator} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
