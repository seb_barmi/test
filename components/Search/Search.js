import React from 'react';

import { View, Text, TextInput, StyleSheet, Image } from 'react-native';
import Magnifier from '../../assests/icons/Vector.png'

const Search = () => {
    return (
        <View style={styles.search}>
            <TextInput style={styles.searchInput} placeholder="Ищу то самое"/>
            <Image style={styles.magnifier} source={Magnifier}/>
        </View>
    );
};


const styles = StyleSheet.create({
 search: {
   marginTop: 16
 },
 searchInput: {
     backgroundColor: '#F6F6F6',
    borderRadius: 6,
    position: 'relative',
    paddingVertical: 7,
    paddingRight: 16,
    paddingLeft: 34,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 13,
    lineHeight: 18,
    letterSpacing: -0.08,
    color: '#989FA6',

 },
 magnifier: {
    position: 'absolute',
    top: '32%',
    left: 8,
    width: 16,
    height: 16
 }
})

export default Search;