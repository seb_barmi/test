import React, {useState} from 'react';

import {SafeAreaView, View, TextInput, Text, Button} from 'react-native';
import dataCoffee from '../../services/dataService';

const setDataTranslation = new dataCoffee();

const Translator = () => {
  const [lang, setLang] = useState('');
  const [text, setText] = useState('');
  const [translateLang, setTranslateLang] = useState('');
  const [translateText, setTranslateText] = useState('Ваш перевод будет тут');

  const setTextTranslate = () => {
    setDataTranslation
      .setDataTranslate(text, lang, translateLang)
      .then(res => setTranslateText(res));
  };

  return (
    <SafeAreaView>
      <View>
        <TextInput
          onChangeText={setLang}
          value={lang}
          placeholder="Введите язык"
          keyboardType="default"
        />
        <TextInput
          onChangeText={setText}
          value={text}
          placeholder="Введите текст"
          keyboardType="default"
        />
        <TextInput
          onChangeText={setTranslateLang}
          value={translateLang}
          placeholder="Введите язык на который нужно перевести"
          keyboardType="default"
        />
        <Text>{translateText}</Text>
        <Button title="Перевести" onPress={setTextTranslate} />
      </View>
    </SafeAreaView>
  );
};

export default Translator;
