import React from 'react';
import {View, Text, Image, StyleSheet, ImageBackground} from 'react-native';
import Button from '../Button';

const MenuListItem = ({item: {name, size, ImageURL, price, done, sale}}) => {
  return (
    <View style={styles.menuListItem}>
      <View style={[styles.menuListItemImageWrapper]}>
        <ImageBackground
          source={ImageURL}
          resizeMode="cover"
          style={[
            {backgroundColor: done ? 'rgba(0,0,0,0.5)' : 'rgba(0,0,0,0)'},
            styles.menuListItemImage,
          ]}
          imageStyle={{
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            opacity: done ? 0.5 : 1,
          }}>
          <Text style={styles.text}>{done ? 1 : null}</Text>
        </ImageBackground>
      </View>
      <View style={styles.menuListItemWrap}>
        <View style={styles.menuListItemTitle}>
          <Text style={styles.menuListItemName}>{name}</Text>
          <Text style={styles.menuListItemSize}>{size}</Text>
        </View>
        <View style={styles.menuListItemBuy}>
          <Button
            isFall
            children={
              done ? (
                <Image source={require('../../assests/icons/done.png')} />
              ) : null
            }
            style={styles.menuListItemBtn}
            price={price}
            done={done}
          />
          <Text style={styles.menuListItemPrice}>{price} ₽</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  menuListItem: {
    marginRight: 15,
    flex: 1,
    borderRadius: 18,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.08,
    shadowRadius: 24,
  },
  menuListItemImageWrapper: {},
  menuListItemImage: {
    height: 112,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  text: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 29,
    color: '#FFFFFF',
  },
  menuListItemName: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 19,
    color: '#353539',
  },
  menuListItemWrap: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 10,
    paddingBottom: 10,
    justifyContent: 'space-between',
  },
  menuListItemSize: {
    width: 120,
    marginTop: 6,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 13,
    lineHeight: 18,
    letterSpacing: -0.08,
    color: '#989FA6',
  },
  menuListItemBuy: {
    marginTop: 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuListItemBtn: {
    width: 61,
    height: 25,
    backgroundColor: '#4A4A49',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
  },
  menuListItemPrice: {
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: -0.32,
    textDecorationLine: 'line-through',
    color: '#989FA6',
  },
});

export default MenuListItem;
