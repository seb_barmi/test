import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MenuListItem from '../MenuListItem';
import dataCoffee from '../../services/dataService';

const dataWithMenu = new dataCoffee();

const MenuList = () => {
  return (
    <View style={styles.menuList}>
      {dataWithMenu.getMenu().drinks.map(item => {
        return <MenuListItem key={item.id} item={item} />;
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  menuList: {
    marginTop: 20,
    flexDirection: 'row',
    marginBottom: 10,
    flexWrap: 'wrap',
  },
});

export default MenuList;
