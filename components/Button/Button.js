import React from 'react';

import {TouchableOpacity, View, Text, Image, StyleSheet} from 'react-native';

const Button = ({children, price, style, done, refresh}) => {
  const content =
    children && done ? (
      children
    ) : (
      <Image
        style={styles.buttonIcon}
        source={require('../../assests/icons/refresh.png')}
      />
    );

  return (
    <View style={[styles.button, style]}>
      <TouchableOpacity style={styles.buttonOp}>
        {refresh ? content : null}
        {!done && !children ? null : content}
        {price && !done ? (
          <Text
            style={[
              styles.buttonText,
              {marginLeft: price && refresh ? 11 : 0},
            ]}>
            {price}
          </Text>
        ) : null}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    width: 93,
    height: 25,
    backgroundColor: '#4A4A49',
    borderRadius: 4,
  },
  buttonOp: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 3,
  },
  buttonText: {
    textAlign: 'center',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: -0.32,
    color: '#FFFFFF',
    marginLeft: 11,
  },
});

export default Button;
