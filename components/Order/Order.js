import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Button from '../Button';
import AddressText from '../AdressText';
import OrderList from '../OrderList';
import Title from '../Title';

const Order = () => {
  return (
    <View style={styles.order}>
      <View style={styles.orderTitleWrapper}>
        <Title text="Прошлый заказ" />
        <Button refresh done={false} price="1 750 ₽" />
      </View>
      <AddressText
        text="Анкудиновка, проспект Героя, 22"
        style={styles.addressText}
      />
      <OrderList />
    </View>
  );
};

const styles = StyleSheet.create({
  order: {
    marginTop: 25,
  },
  orderTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderTitle: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 24,
    color: '#353539',
  },
  addressText: {
    marginTop: 2,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 13,
    lineHeight: 18,
    letterSpacing: -0.08,
    color: '#989FA6',
  },
});

export default Order;
