import React from 'react';

import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const ShoppingCartBtn = ({style, price}) => {
  return (
    <View style={[styles.cart, style]}>
      <TouchableOpacity style={styles.cartWrap}>
        <View style={styles.cartInside}>
          <Text style={styles.cartText}>Корзина</Text>
          <Text style={styles.cartPrice}>{price}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  cart: {
    width: 198,
    height: 44,
    backgroundColor: '#4A4A49',
    borderRadius: 6,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  cartInside: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cartText: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 17,
    lineHeight: 20,
    letterSpacing: -0.32,
    color: '#FFFFFF',
  },
  cartPrice: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 17,
    lineHeight: 20,
    letterSpacing: -0.32,
    color: '#FFFFFF',
  },
});

export default ShoppingCartBtn;
