import React from 'react';
import {Text, StyleSheet} from 'react-native';

const Title = ({text, style}) => (
  <Text style={styles.title}>{text}</Text>
);

const styles = StyleSheet.create({
  title: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 24,
    color: '#353539',
  },
});

export default Title;
