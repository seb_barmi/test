import React from 'react';

import { Text } from 'react-native';

const AdressText = ({text, style}) => <Text style={style}>{text}</Text>;

export default AdressText;
