import React from 'react';
import {View, StyleSheet} from 'react-native';
import OrderListItem from '../OrderListItem';
import dataCoffee from '../../services/dataService';

const dataWithOrders = new dataCoffee();

const OrderList = () => {
  return (
    <View style={styles.orderList}>
      {dataWithOrders.getOrder().map(item => {
        return <OrderListItem key={item.id} item={item} />;
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  orderList: {
    marginTop: 16,
  },
});

export default OrderList;
