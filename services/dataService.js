export default class dataCoffee {
  getStories = () => {
    return [
      {
        id: 0,
        text: 'Короткий заголовок',
        imageURL: require('../assests/storyImage/1.png'),
      },
      {
        id: 1,
        text: 'Кофейня — сторис',
        imageURL: require('../assests/storyImage/2.png'),
      },
      {
        id: 2,
        text: 'Кофейня — сторис',
        imageURL: require('../assests/storyImage/3.png'),
      },
      {
        id: 3,
        text: 'Короткий заголовок',
        imageURL: require('../assests/storyImage/4.png'),
      },
    ];
  };

  getOrder = () => {
    return [
      {id: 0, name: 'Американо', price: 250, size: '200 мл'},
      {id: 1, name: 'Лимонный пай без сахара', price: 250, size: '110 г'},
    ];
  };

  getMenu = () => {
    return {
      drinks: [
        {
          id: 0,
          name: 'Американо',
          size: '200 мл / 250 мл / 400 мл',
          price: 150,
          sale: 300,
          ImageURL: require('../assests/storyImage/1.png'),
          done: true,
        },
        {
          id: 1,
          name: 'Капучино на кокосовом',
          size: '200 мл / 250 мл / 400 мл',
          price: 150,
          ImageURL: require('../assests/storyImage/2.png'),
        },
      ],
    };
  };

  setDataTranslate = (text, lang, translateLang) => {
    return fetch(
      'https://deep-translate1.p.rapidapi.com/language/translate/v2',
      {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'x-rapidapi-key':
            'd1583ccd17msh56304de46ef93bdp1cdc3ajsnb3b75738a76d',
          'x-rapidapi-host': 'deep-translate1.p.rapidapi.com',
        },
        body: JSON.stringify({
          q: text,
          source: lang,
          target: translateLang,
        }),
      },
    )
      .then(res => res.json())
      .then(body => body.data.translations.translatedText)
      .catch(err => {
        console.error(err);
      });
  };
}
